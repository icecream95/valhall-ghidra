# Valhall processor module for Ghidra

This repository contains a Ghidra processor module for the ARM Mali
Valhall GPU.

## Installing Ghidra

Ghidra requires OpenJDK, which can be installed via the `default-jdk`
package on Debian.

Download Ghidra from [the release
page](https://github.com/NationalSecurityAgency/ghidra/releases). For
running on AArch64, 10.1 or later is required.

Unzip the release archive.

On AArch64, use the `./support/buildNatives` script to make a native
build of the decompiler. This requires a version of `gradle` newer
than in Debian repositories to be installed, but it is possible to
compile it manually, with some effort.

Alternatively, download the tarball with binaries from
https://gitlab.com/icecream95/valhall-ghidra/-/snippets/2208224 and
extract it from the root Ghidra directory.

## Download repository

From the Ghidra directory:

```sh
git clone https://gitlab.com/icecream95/valhall-ghidra Ghidra/Processors/Valhall
```

This repository has not been packaged as a proper Ghidra module
because it is still very incomplete, and updating is eaiser when the
module is installed this way.

## Usage

Starting Ghidra:

```sh
./ghidraRun
```

Start by creating a project, then importing a raw binary file of
Valhall code. Select the button to the right of "Language", and choose
the Valhall processor.

With the file selected in the project window, hit Enter to open the
Code Browser.

To start disassembly, select a byte in the listing and hit `d`.

If the disassembly fields are too narrow, there is a button at the top
of the Listing subwindow to "Edit the Listing fields".

## Syntax differences

There are some syntax differences between the syntax used here and by
other disassemblers.

So far, the main one is that 64-bit registers are prefixed with "x",
and that 64-bit push uniform loads use "uu".

## Development

In `Window->Script Manager`, enable `ReloadSleighLanguage` and add a
key binding for the script.

To change the processor definition, edit `Valhall.slaspec.mako`, then
render a new version of the SLEIGH file, and finally use the key bind
you set to reload it with your changes.

```sh
mako-render Valhall.slaspec.mako >Valhall.slaspec
```

To check for errors in the slaspec file, either use the sleigh binary
at `Ghidra/Features/Decompiler/build/os/linux_arm_64/sleigh`, passing
the slaspec file as an argument, or use the `support/sleigh` script
for nicer error messages at the expense of speed.

HTML documentation for the SLEIGH specification file format is found
in `docs/languages`.

Note that the reload script is not perfect: Larger changes (for
example, changing register names) may cause Ghidra to act oddly until
you quit and restart it.

## Getting Valhall binaries

If you have hardware set up with the blob, you should know how to get
binaries from it yourself.

The assembler in panwrap can also be used: the binary it outputs can
be imported directly.

## Running the blob without hardware

First, you'll need the blob itself. I've tested with the [Chrome OS
asurada blob](http://commondatastorage.googleapis.com/chromeos-localmirror/distfiles/mali-drivers-valhall-asurada-25.0_p33.run).
Then, you'll need a suitable libc, which you can get (with a free dEQP
build) from
https://gitlab.com/icecream95/valhall-ghidra/-/snippets/2209710

(If you want to set things up yourself, use panloader from
[https://gitlab.com/icecream95/panloader](https://gitlab.com/icecream95/panloader))

That branch of panloader will dump any shaders it encounters to
`/tmp/vallhall-shader-<N>.bin`, so they can then be imported into
Ghidra.

## Updating machine code

While it is possible to reimport new files, if the assembly file has
not increased in size then it can be faster to paste in the new bytes
directly. Use this pipeline and pipe it to your favourite clipboard
tool to copy a binary file in a format suitable for Ghidra:

```sh
od -t x1 dump.bin | cut -d" " -f2- -s | tr '\n' ' ' | sed 's/ $/\n/'
```

Select the region you want to paste into (use `Ctrl-A` to select all)
and press `c` to clear the existing instructions. Then move the cursor
to the byte to start inserting the new instructions, and press
`Ctrl-V` to paste. Then press `d` to disassemble the program again.

Ghidra also has an integrated assembler which should work in many
cases, used from the "Patch Instruction" context menu item.
