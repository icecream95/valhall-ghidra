<%!
src_ranges = (
    ("a", 0, 0),
    ("r", 0, 5),
    ("r_64", 1, 5),
    ("u", 0, 5),
    ("u_64", 1, 5),
    ("uv", 0, 5),
    ("uv_64", 1, 5),
    ("i", 0, 4),
    ("ts", 0, 2),
    ("id", 0, 4),
    ("bl", 0, 4),
    ("tl", 5, 7),
    ("t", 6, 7),
)

swizzles = (
    ("SW", "64", "widen",
     (None, None),
     (None,),
     (".h0", "0,16"),
     (".h1", "16,16"),
     (".b0", "0,8"),
     (".b1", "8,8"),
     (".b2", "16,8"),
     (".b3", "24,8"),
     (".w0", "0,32"),
     ),
    ("SW", "32", "widen",
     (None, None),
     (None,),
     (".h0", "0,16"),
     (".h1", "16,16"),
     (".b0", "0,8"),
     (".b1", "8,8"),
     (".b2", "16,8"),
     (".b3", "24,8"),
     ),
    ("SW", "16", "widen",
     (".h00", "0,16", "0,16"),
     (".h10", "16,16", "0,16"),
     (None, None),
     (".h11", "16,16", "16,16"),
     (".b00", "0,8", "0,8"),
     (".b20", "16,8", "0,8"),
     (".b02", "0,8", "16,8"),
     (".b22", "16,8", "16,8"),
     (".b11", "8,8", "8,8"),
     (".b31", "24,8", "8,8"),
     (".b13", "8,8", "24,8"),
     (".b33", "24,8", "24,8"),
     (".b01", "0,8", "8,8"),
     (".b23", "16,8", "24,8"),
     ),
    ("SW", "8", "widen",
     (None, None),
     (".b3210", "24,8", "16,8", "8,8", "0,8"),
     (".b0101", "0,16", "0,16"),
     (".b2323", "16,16", "16,16"),
     (".b0000", "0,8", "0,8", "0,8", "0,8"),
     (".b1111", "8,8", "8,8", "8,8", "8,8"),
     (".b2222", "16,8", "16,8", "16,8", "16,8"),
     (".b3333", "24,8", "24,8", "24,8", "24,8"),
     (".b2301", "16,16", "0,16"),
     (".b1032", "8,8", "0,8", "24,8", "16,8"),
     (".b0011", "0,8", "0,8", "8,8", "8,8"),
     (".b2233", "16,8", "16,8", "24,8", "24,8"),
     ),
    ("SW", "lane32", "lane",
     (None, None),
     (".h0", "0,16"),
     (".h1", "16,16"),
     ),
    ("SW", "swz32", "swz",
     (None, None),
     (".h0", "0,16"),
     (".h1", "16,16"),
     ),
    ("SW", "lane8", "widen",
     (".b02", "0,8", "16,8"),
     (None,),
     (None,),
     (None,),
     (".b00", "0,8", "0,8"),
     (".b11", "8,8", "8,8"),
     (".b22", "16,8", "16,8"),
     (".b33", "24,8", "24,8"),
     (None,),
     (None,),
     (".b01", "0,8", "8,8"),
     (".b23", "16,8", "24,8"),
     ),
    ("FW", "32", "swz",
     (None, None),
     (".h0", "0,16"),
     (".h1", "16,16"),
     ),
    ("FW", "16", "swz",
     (".h00", "0,16", "0,16"),
     (".h10", "16,16", "0,16"),
     (None, None),
     (".h11", "16,16", "16,16"),
     ),
)

load_lanes = {
   8: ("b0", "b1", "b2", "b3", "h0", "h1", "w0", "d0"),
   16: ("h0", "h1", "w0", "d0"),
   32: (None, "d0"),
}

round_mode = (
   (".rte", "round"),
   (".rtp", "ceil"),
   (".rtn", "floor"),
   (".rtz", ""),
)

convert = (
   (0x00, "S8_TO_S32", "S1_C8", False, "sext({})"),
   (0x01, "S8_TO_F32", "S1_C8", False, "int2float({})"),
   (0x02, "S8_TO_S16", "S1_C8", False, ":2 sext({})"),
   (0x03, "S8_TO_F16", "S1_C8", False, ":2 int2float({})"),

   (0x10, "U8_TO_U32", "S1_C8", False, "zext({})"),
   (0x11, "U8_TO_F32", "S1_C8", False, "int2float({})"),
   (0x12, "U8_TO_U16", "S1_C8", False, ":2 zext({})"),
   (0x13, "U8_TO_F16", "S1_C8", False, ":2 int2float({})"),

   # TODO: Do these really use widen rather than lane selection?
   (0x04, "S16_TO_S32", "SW1_16", False, "sext({}:2)"),
   (0x05, "S16_TO_F32", "SW1_16", False, "int2float({}:2)"),
#  (0x07, "V2S16_TO_V2F16", "SW1_32", False, "int2float({})"),
   (0x09, "S32_TO_F32", "SW1_32", False, "int2float({})"),

   (0x14, "U16_TO_U32", "SW1_16", False, "zext({}:2)"),
   (0x15, "U16_TO_F32", "SW1_16", False, "int2float({}:2)"),
#  (0x17, "V2U16_TO_V2F16", "SW1_32", False, "int2float({})"),
   (0x19, "U32_TO_F32", "SW1_32", False, "int2float({})"),

   (0x0C, "F32_TO_S32", "F1", True, "trunc(round({}))"),
   (0x1C, "F32_TO_U32", "F1", True, "trunc(round({}))"),

#  (0x0E, "V2F16_TO_V2S16", "F1", True, "round({})"),
#  (0x1E, "V2F16_TO_V2S16", "F1", True, "round({})"),
   (0x0A, "F16_TO_S32", "F1_16", True, "trunc(round({}[0,16]))"),
   (0x1A, "F16_TO_U32", "F1_16", True, "trunc(round({}[0,16]))"),

   (0x0B, "F16_TO_F32", "F1_16", False, "float2float({}[0,16])"),
   (0x0D, "FROUND.f32", "F1_16", True, "round({})"),
   (0x0F, "FROUND.v2f16", "F1_16", True, "local val:4; local src:4 = {}; val[0,16] = round(src[0,16]); val[16,16] = round(src[16,16])"),
)

# TODO: What does this even do?
total_cmp = "total({a}, {b}) == 1:1"

compare = (
    (".eq", 0, "==", "==", "f=="),
    (".gt", 1, ">", "s>", "f>"),
    (".ge", 2, ">=", "s>=", "f>="),
    (".ne", 3, "!=", "!=", "f!="),
    (".lt", 4, "<", "s<", "f<"),
    (".le", 5, "<=", "s<=", "f<="),
    (".gtlt", 6, "!=", "!=", "({a} f< {b}) || ({b} f> {a})"),
    (".total", 7, total_cmp, total_cmp, total_cmp),
)

B = ""
E = "build M;"

%>

define endian=little;
define alignment=8;

define space ram type=ram_space size=8 default;
define space register type=register_space size=4;

define token opword (64)
% for src in range(1, 5):
<% base = (src - 1) * 8 %>
        src${src} = (${base},${base + 7})
% for n, s, e in src_ranges:
        src${src}${n} = (${base+s},${base+e})
% endfor
% endfor

        swz1         = (28,29)
        swz2         = (26,27)
        swz3         = (24,25)
        swz4         = (22,23)

        absneg1      = (38,39)
        absneg2      = (36,37)
        absneg3      = (34,35)
        absneg4      = (32,33)

        widen1       = (36,39)
        widen2       = (26,29)
        lane1        = (37,38)
        cvt_lane8    = (28,29)
        cvt_lane16   = (28,28)

        mod_explicit = (11,11)
        mod_shadow   = (12,12)
        mod_lod_mode = (13,15)
        mod_left     = (32,32)
        mod_shift    = (20,22)
        mod_and      = (24,24)
        mod_regfmt   = (24,26)
        mod_seq      = (25,25)
        mod_dim      = (28,29)
        mod_vecsize  = (28,29)
        mod_sat      = (30,30)
        mod_not_res  = (30,30)
        mod_rhadd    = (30,30)
        mod_restype  = (30,31)
        mod_round    = (30,31)
        mod_cmp      = (32,34)
        mod_not_src  = (35,35)
        mod_eq       = (36,36)
        mod_skip     = (39,39)
        mod_unsigned = (39,39)

        D1           = (40,45)
        D1_a         = (40,40)
        D1_64        = (41,45)
        DM           = (46,47)

        op           = (48,56)
        op2          = (16,19)
        op2_cvt      = (16,20)
        op2_ldst     = (27,29)
        op2_shift    = (24,25)

        sr_count     = (33,35)
        sr1_1        = (40,45)
        sr1_2        = (40,45)
        sr1_3        = (40,45)
        sr1_4        = (40,45)
        sr2_1        = (16,21)
        sr2_2        = (16,21)
        sr2_3        = (16,21)
        sr2_4        = (16,21)

        load_lane    = (36,38)

        clamp        = (32,33)

        branchz_ofs  = (8,34) signed
        ldst_ofs     = (8,23) signed
        imm_32       = (8,39)
        blend_target = (8,15)
        lea_imm      = (8,11)
        ld_var_index = (20,23)

        unk36        = (36,39)

        imm_mode     = (57,58)
        action       = (59,61)
        do_action    = (62,62)

        pseudo_op    = (0,7)
        pseudo_imm   = (8,39)
        pseudo_wr    = (40,41)
;

<%!
   def format_reglist(prefix, seq, pred=None):
      ret = ""
      for num, r in enumerate(seq):
         if not num & 15:
            ret += "\n       "
         if pred and not pred(r):
            ret += " _"
            continue
         ret += f" {prefix}{r}"
      return ret
 %>

define register offset=0x00 size=8 [
${format_reglist("x", range(0, 64, 2))}
];

define register offset=0x100 size=8 [
${format_reglist("uu", range(0, 64, 2))}
];

define register offset=0x00 size=4 [
${format_reglist("r", range(64))}
];

define register offset=0x100 size=4 [
${format_reglist("u", range(64))}
];

define register offset=0x200 size=8 [
        PC SP FauBase ResourceTables CounterLow
];

define register offset=0x300 size=4 [
        tls_ptr tls_ptr_hi wls_ptr wls_ptr_hi
        lane_id core_id program_counter

        atest_datum
        ${format_reglist("blend_descriptor_", [f"{n}_{v}" for n in range(8) for v in ("x", "y")])}
];

attach variables [ src1r src2r src3r src4r D1 sr1_1 sr2_1 ] [
${format_reglist("r", range(64))}
];

attach variables [ src1u src2u src3u src4u ] [
${format_reglist("u", range(64))}
];

attach variables [ src1r_64 src2r_64 src3r_64 src4r_64 D1_64 ] [
${format_reglist("x", range(0, 64, 2))}
];

attach variables [ src1u_64 src2u_64 src3u_64 src4u_64 ] [
${format_reglist("uu", range(0, 64, 2))}
];

% for sr in range(1, 4):
attach variables [ sr1_${sr+1} sr2_${sr+1} ] [
${format_reglist("r", range(sr, 64))}
% for reg in range(sr):
        _
% endfor
];
% endfor

attach values [ src1i src2i src3i src4i ] [
        0x0 0xffffffff 0x7fffffff 0xfafcfdfe 0x1000000 0x80002000
        0x70605030 0xc0b0a090 0x3020100 0x7060504 0xb0a0908 0xf0e0d0c
        0x13121110 0x17161514 0x1b1a1918 0x1f1e1d1c 0x3f800000
        0x3dcccccd 0x3ea2f983 0x3f317218 0x40490fdb 0x0 0x477fff00
        0x5c005bf8 0x2e660000 0x34000000 0x38000000 0x3c000000
        0x40000000 0x44000000 0x48000000 0x42480000
];

attach variables [ src1ts src2ts src3ts src4ts ] [
        _ _ tls_ptr tls_ptr_hi _ _ wls_ptr wls_ptr_hi
];

attach variables [ src1id src2id src3id src4id ] [
        _ _ lane_id _ _ _ core_id _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
        _ _ _ _ _ program_counter _
];

attach variables [ src1bl src2bl src3bl src4bl ] [
        _ _ _ _ _ _ _ _ _ _ atest_datum _ _ _ _ _
${format_reglist("blend_descriptor_", [f"{n}_{v}" for n in range(8) for v in ("x", "y")])}
];

M: "" is action=0 & do_action=0 { }
M: ".wait0" is action=1 & do_action=0 { }
M: ".wait1" is action=2 & do_action=0 { }
M: ".wait01" is action=3 & do_action=0 { }
M: ".wait2" is action=4 & do_action=0 { }
M: ".wait02" is action=5 & do_action=0 { }
M: ".wait12" is action=6 & do_action=0 { }
M: ".wait012" is action=7 & do_action=0 { }
M: ".wait0126" is action=0 & do_action=1 { }
M: ".barrier" is action=1 & do_action=1 { }
M: ".reconverge" is action=2 & do_action=1 { }
M: ".td" is action=5 & do_action=1 { }
M: ".return" is action=7 & do_action=1 { ret:8 = -1; return [ret]; }

DEST: D1^".mask0" is D1 & DM=0 { export D1; }
DEST: D1^".h0" is D1 & DM=1 { export D1; }
DEST: D1^".h1" is D1 & DM=2 { export D1; }
DEST: D1 is D1 & DM=3 { export D1; }

macro pushRead(addr, dest) {
   dest = *:4 (FauBase + addr * 4:8);
}

macro pushRead_64(addr, dest) {
   dest = *:8 (FauBase + addr * 8:8);
}

% for s in range(1, 5):

# 64-bit registers must be aligned
S${s}_64: "ERR" is src${s}a=1 & src${s}t!=3 unimpl

# TODO: This code needs a major cleanup

% for suf in ("", "_64"):
<%
   r = f"src{s}r{suf}"
   u = f"src{s}u{suf}"
   pred = f" & src{s}a=0" if suf == "_64" else ""
   suf_sz = 8 if suf == "_64" else 4
   pred_u = pred + f" & src{s}uv{suf}"
   read_u = f"local temp:{suf_sz}; pushRead{suf}(src{s}uv{suf}:8, temp)"
%>
S${s}${suf}: ${r} is ${r} & src${s}t=0${pred} { export ${r}; }
S${s}${suf}: "`"^${r} is ${r} & src${s}t=1${pred} { export ${r}; }
S${s}${suf}: ${u} is ${u} & src${s}t=2${pred_u} { ${read_u}; export temp; }

# TODO: How do immediates work in 64-bit instructions?
S${s}${suf}: src${s}i is src${s}i & src${s}t=3 & imm_mode=0 { export *[const]:${suf_sz} src${s}i; }

% if suf == "_64":
S${s}${suf}: src${s}ts is src${s}ts & src${s}tl=7 & imm_mode=1 { local temp:8; temp = zext(src${s}ts); export temp; }
S${s}${suf}: src${s}id is src${s}id & src${s}tl=7 & imm_mode=3 { local temp:8; temp = zext(src${s}id); export temp; }
S${s}${suf}: src${s}bl is src${s}bl & src${s}tl=7 & imm_mode=0 { local temp:8; temp = zext(src${s}bl); export temp; }
% else:
S${s}${suf}: src${s}ts is src${s}ts & src${s}tl=7 & imm_mode=1 { export src${s}ts; }
S${s}${suf}: src${s}id is src${s}id & src${s}tl=7 & imm_mode=3 { export src${s}id; }
S${s}${suf}: src${s}bl is src${s}bl & src${s}tl=7 & imm_mode=0 { export src${s}bl; }
% endif
% endfor

% for pre, bits, var, *patterns in swizzles:
% for widen, (name, *ranges) in enumerate(patterns):
<%
   if not len(ranges): continue
   if var == "lane" and s != 1: continue
   if var != "swz" and s > 2: continue
   name = f'^"{name}"' if name else ""
   if bits == "64":
      total_size = 64
      tempsize = 8
   else:
      total_size = 32
      tempsize = 4
   size = total_size // len(ranges)
   code = ""
   for num, r in enumerate(ranges):
      assign = f"[{num*size},{size}]" if size != total_size else ""
      if r and int(r.split(",")[1]) == size:
         val = f"S{s}[{r}]"
      elif r and pre == "FW":
         val = f"float2float(S{s}[{r}])"
      elif r:
         val = f"zext(S{s}[{r}])"
      elif bits == "64":
         # TODO: Read as 64-bit
         val = f"zext(S{s})"
      else:
         val = f"S{s}"
      code += f"temp{assign} = {val}; "
 %>${pre}${s}_${bits}: S${s}${name} is S${s} & ${var}${s}=${widen} { local temp:${tempsize}; ${code}export temp; }
% endfor

% endfor

F${s}: FW${s}_32 is FW${s}_32 & absneg${s}=0 { export FW${s}_32; }
F${s}: FW${s}_32^".neg" is FW${s}_32 & absneg${s}=1 { t:4 = 0:4 f- FW${s}_32; export t; }
F${s}: FW${s}_32^".abs" is FW${s}_32 & absneg${s}=2 { t:4 = abs(FW${s}_32); export t; }
F${s}: FW${s}_32^".neg.abs" is FW${s}_32 & absneg${s}=3 { t:4 = 0:4 f- abs(FW${s}_32); export t; }

F${s}_16: FW${s}_16 is FW${s}_16 & absneg${s}=0 { export FW${s}_16; }
F${s}_16: FW${s}_16^".neg" is FW${s}_16 & absneg${s}=1 {
   local t:4;
   t[0,16] = 0:2 f- FW${s}_16[0,16];
   t[16,16] = 0:2 f- FW${s}_16[16,16];
   export t;
}
F${s}_16: FW${s}_16^".abs" is FW${s}_16 & absneg${s}=2 {
   local t:4;
   t[0,16] = abs(FW${s}_16[0,16]);
   t[16,16] = abs(FW${s}_16[16,16]);
   export t;
}
F${s}_16: FW${s}_16^".neg.abs" is FW${s}_16 & absneg${s}=3 {
   local t:4;
   t[0,16] = 0:2 f- abs(FW${s}_16[0,16]);
   t[16,16] = 0:2 f- abs(FW${s}_16[16,16]);
   export t;
}

% endfor

S1_C8: S1^".b0" is S1 & cvt_lane8=0 { local tmp:1 = S1[0,8]; export tmp; }
S1_C8: S1^".b1" is S1 & cvt_lane8=1 { local tmp:1 = S1[8,8]; export tmp; }
S1_C8: S1^".b2" is S1 & cvt_lane8=2 { local tmp:1 = S1[16,8]; export tmp; }
S1_C8: S1^".b3" is S1 & cvt_lane8=3 { local tmp:1 = S1[24,8]; export tmp; }

S1_C16: S1^".h0" is S1 & cvt_lane16=0 { local tmp:1 = S1[0,16]; export tmp; }
S1_C16: S1^".h1" is S1 & cvt_lane16=1 { local tmp:1 = S1[16,16]; export tmp; }

LdstAddr: S1_64, "offset":ldst_ofs is S1_64 & ldst_ofs { local ptr:8 = S1_64; ptr = ptr + ldst_ofs:8; export ptr; }

# TODO: Do something with the mode descriptor?

BufferAddr: S1, S2 is S1 & S2 {
        # TODO: Read buffer contents if top byte is.. zero?
        # (I've seen 0x3E for reading descriptors, 0x7E has also been
        # documented to occur)
        local buf:1 = S2:1;
        # TODO: The pointer should be 56-bits
        # Maybe we can make use of memory-tagging support to ignore
        # the top bits?
        local bufaddr:8 = ResourceTables + zext(buf) * 16:8;
        local buffers:4 = *:4 bufaddr;
        local ptr:8 = zext(buffers + S1);
        export ptr;
}

# Comparison bits: LT=1 EQ=2 GT=4 NE=8
CMP: ".eq" is mod_cmp=0 { export 2:1; }
CMP: ".gt" is mod_cmp=1 { export 4:1; }
CMP: ".ge" is mod_cmp=2 { export 6:1; }
CMP: ".ne" is mod_cmp=3 { export 8:1; }
CMP: ".lt" is mod_cmp=4 { export 1:1; }
CMP: ".le" is mod_cmp=5 { export 3:1; }
CMP: ".gtlt" is mod_cmp=6 { export 5:1; }
# TODO: What does total even do?
CMP: ".total" is mod_cmp=7 unimpl

RESTYPE: ".i1" is mod_restype=0 { export 1:4; }
RESTYPE: ".f1" is mod_restype=1 { export 0x3f800000:4; }
RESTYPE: ".m1" is mod_restype=2 { export 0xffffffff:4; }
RESTYPE: ".u1" is mod_restype=3 { export 0:4; }

RESTYPE_16: ".i1" is mod_restype=0 { export 1:2; }
RESTYPE_16: ".f1" is mod_restype=1 { export 0x3C00:2; }
RESTYPE_16: ".m1" is mod_restype=2 { export 0xffff:2; }
RESTYPE_16: ".u1" is mod_restype=3 { export 0:2; }

SAT: "" is mod_sat=0 { export 0:1; }
SAT: ".saturate" is mod_sat=1 { export 1:1; }

RHADD: "" is mod_rhadd=0 { export 0:1; }
RHADD: ".rhadd" is mod_rhadd=1 { export 1:1; }

LOD_MODE: ".zero" is mod_lod_mode=0 { export 0:1; }
LOD_MODE: ".computed" is mod_lod_mode=1 { export 1:1; }
LOD_MODE: ".explicit" is mod_lod_mode=4 { export 4:1; }
LOD_MODE: ".computed_bias" is mod_lod_mode=5 { export 5:1; }
LOD_MODE: ".grdesc" is mod_lod_mode=6 { export 6:1; }

DIMENSION: ".1d" is mod_dim=0 { export 0:1; }
DIMENSION: ".2d" is mod_dim=1 { export 1:1; }
DIMENSION: ".3d" is mod_dim=2 { export 2:1; }
DIMENSION: ".cube" is mod_dim=3 { export 3:1; }

SHADOW: "" is mod_shadow=0 { export 0:1; }
SHADOW: ".shadow" is mod_shadow=1 { export 1:1; }

EXPLICIT: "" is mod_explicit=0 { export 0:1; }
EXPLICIT: ".explicit_offset" is mod_explicit=1 { export 1:1; }

SKIP: "" is mod_skip=0 { export 0:1; }
SKIP: ".skip" is mod_skip=1 { export 1:1; }

SIGN: "i" is mod_unsigned=0 { export 1:1; }
SIGN: "u" is mod_unsigned=1 { export 0:1; }

EQ: "" is mod_eq=0 { export 0:1; }
EQ: ".eq" is mod_eq=1 { export 1:1; }

VECSIZE: "" is mod_vecsize=0 { export 1:1; }
VECSIZE: ".v2" is mod_vecsize=1 { export 2:1; }
VECSIZE: ".v3" is mod_vecsize=2 { export 3:1; }
VECSIZE: ".v4" is mod_vecsize=3 { export 4:1; }

REGFMT: ".f32" is mod_regfmt=2 { export 2:1; }
REGFMT: ".f16" is mod_regfmt=3 { export 3:1; }
REGFMT: ".s32" is mod_regfmt=4 { export 4:1; }
REGFMT: ".s16" is mod_regfmt=5 { export 5:1; }
REGFMT: ".u16" is mod_regfmt=6 { export 6:1; }
REGFMT: ".u16" is mod_regfmt=7 { export 7:1; }

NOT_RES: "" is mod_not_res=0 { export 0:1; }
NOT_RES: ".not_result" is mod_not_res=1 { export 1:1; }

NOT_SRC: "" is mod_not_src=0 { export 0:1; }
NOT_SRC: ".not" is mod_not_src=1 { export 1:1; }

AND: "" is mod_and=0 { export 0:1; }
AND: ".and" is mod_and=1 { export 1:1; }

macro Store(reg, mask, val) {
      maskv = (mask & 1) * 0xffff + (mask & 2) * 0x7fff8000;
      reg = (val & maskv) | (reg & ~maskv);
}

# TODO: Do masks work like this on 64-bit instructions?
macro Store64(reg, mask, val) {
      maskv = (mask & 1) * 0xffffffff + (mask & 2) * 0x7fffffff80000000;
      reg = (val & maskv) | (reg & ~maskv);
}

macro StoreClamp(reg, mask, clamp, val) {
      # if bit 1 is set, clamp to -1
      if ((val f>= 0xbf800000:4) || ((clamp & 1:1) == 0:1)) goto <no_clamp_m1>;
      val = 0xbf800000:4;
    <no_clamp_m1>

      # if bit 2 is set, clamp to 0
      if ((val f>= 0:4) || ((clamp & 2:1) == 0:1)) goto <no_clamp_0>;
      val = 0:4;
    <no_clamp_0>

      # if bit 3 is set, clamp to 1
      if ((val f<= 0x3f800000:4) || ((clamp & 4:1) == 0:1)) goto <no_clamp_1>;
      val = 0x3f800000:4;
    <no_clamp_1>

      Store(reg, mask, val);
}

DC: "" is clamp=0 { export 0:1; }
DC: ".clamp_0_inf" is clamp=1 { export 2:1; }
DC: ".clamp_m1_1" is clamp=2 { export 5:1; }
DC: ".clamp_0_1" is clamp=3 { export 6:1; }

define pcodeop clz;
define pcodeop saturate;
define pcodeop atest;
define pcodeop blend_r;
define pcodeop blend_rg;
define pcodeop blend_rgb;
define pcodeop blend_rgba;
define pcodeop lea_image;
define pcodeop load_var;
define pcodeop load_attr;
define pcodeop blend_coverage;
define pcodeop total;
define pcodeop branchzi;
define pcodeop rsqrt;
define pcodeop store_cvt;

:MOV.i32^M DEST, S1 is op=0x91 & op2=0x0 & M & DEST & DM & S1 {${B}
        Store(DEST, DM, S1);
${E}}
:CLZ.i32^M DEST, S1 is op=0x91 & op2=0x4 & M & DEST & DM & S1 {${B}
        Store(DEST, DM, clz(S1));
${E}}
:IABS.s32^M DEST, S1 is op=0x91 & op2=0x8 & M & DEST & DM & S1 {${B}
        Store(DEST, DM, zext(S1 s> 0) * S1 + zext(S1 s<= 0) * -S1);
${E}}
:NOT.i32^M DEST, S1 is op=0x91 & op2=0xE & M & DEST & DM & S1 {${B}
        Store(DEST, DM, ~S1);
${E}}

:NOP^M is op=0x00 & M {${B} ${E}}

PS_WR: "" is pseudo_wr=0 { export 0:2; }
PS_WR: ".h" is pseudo_wr=1 { export 0x20:2; }
PS_WR: ".w" is pseudo_wr=2 { export 0x2000:2; }

macro psWrite(reg, ps_wr, val) {
        # TODO: Why doesn't this work?
#        reg = (reg >> ps_wr(1)) & ~(0xffffffff:8 << ps_wr:1) | (val << ps_wr:1);
        reg = val;
}

PsBranch32: pseudo_imm is pseudo_imm {
        export *:8 pseudo_imm;
}

# TODO: Support other modes of branch instruction
:branch.w PsBranch32 is op=0 & DM=0 & pseudo_op=1 & PsBranch32 & pseudo_wr=2 {
        goto PsBranch32;
}

:set_fau^PS_WR pseudo_imm is op=0 & DM=0 & pseudo_op=3 & PS_WR & pseudo_imm {
        psWrite(FauBase, PS_WR, pseudo_imm);
}

:set_res^PS_WR pseudo_imm is op=0 & DM=0 & pseudo_op=4 & PS_WR & pseudo_imm {
        psWrite(ResourceTables, PS_WR, pseudo_imm);
}

# TODO: Calculate saturation: check if 64-bit computation == 32-bit computation?
# TODO: CLPER, also shaddx?
# TODO: Finish adding all variants

% for num, op, code in ((0, "IADD", "a + b"), (1, "ISUB", "a - b"), (0xa, "IMUL", "a * b"), (0xb, "HADD", "zext(RHADD == 0) * ((a & b) + ((a ^ b) >> 1)) + zext(RHADD == 1) * ((a | b) - (a ^ b) >> 1)")):
% for saturate in (0, 1):
<%
   if op == "HADD":
      mods = "^RHADD"
      mod_field = "& RHADD"
      code_s = code.replace(">>", "s>>")
      if saturate: continue
   else:
      mods = '^".saturate"' if saturate else ""
      mod_field = f"& mod_sat={saturate}"
      if saturate:
          code = f"saturate({code})"
      code_s = code
 %>

:${op}.u32^M^${mods} DEST, SW1_32, SW2_32 is op=0xA0 & op2=${num} & M & DEST & DM & SW1_32 & SW2_32 ${mod_field} {${B}
        local a = SW1_32; local b = SW2_32;
        Store(DEST, DM, ${code});
${E}}

:${op}.s32^M^${mods} DEST, SW1_32, SW2_32 is op=0xA8 & op2=${num} & M & DEST & DM & SW1_32 & SW2_32 ${mod_field} {${B}
        local a = SW1_32; local b = SW2_32;
        Store(DEST, DM, ${code_s});
${E}}

:${op}.s32^M^${mods} DEST, SW1_32, SW2_32 is op=0x1A0 & op2=${num} & M & DEST & DM & SW1_32 & SW2_32 ${mod_field} {${B}
        local a = SW1_32; local b = SW2_32;
        Store(DEST, DM, ${code_s});
${E}}

:${op}.v2u16^M^${mods} DEST, SW1_16, SW2_16 is op=0xA1 & op2=${num} & M & DEST & DM & SW1_16 & SW2_16 ${mod_field} {${B}
        local temp:4; local a; local b;
        a = SW1_16[0,16]; b = SW2_16[0,16]; temp[0,16] = ${code};
        a = SW1_16[16,16]; b = SW2_16[16,16]; temp[16,16] = ${code};
        Store(DEST, DM, temp);
${E}}

:${op}.s64^M^${mods} DEST, SW1_64, SW2_64 is op=0x1AB & op2=${num} & M & DEST & D1_64 & DM & SW1_64 & SW2_64 ${mod_field} {${B}
        local a = SW1_64; local b = SW2_64;
        Store64(D1_64, DM, ${code});
${E}}

% endfor
% endfor

<%!

float_ops = (
    (0, "FADD", "r = a f+ b;"),
    (2, "FMIN", "if (a f>= b) goto <n{i}>; r = a; goto <e{i}>; <n{i}> r = b; <e{i}>"),
    (3, "FMAX", "if (a f<= b) goto <n{i}>; r = a; goto <e{i}>; <n{i}> r = b; <e{i}>"),
)
 %>

# a4: FADD FMIN FMAX FRSCALE FEXP[32] FADD_LSCALE[32] V2F32_TO_V2F16[16]

% for num, op, code in float_ops:

:${op}.f32^M DEST^DC, F1, F2 is op=0xA4 & op2=${num} & M & DEST & DC & DM & F1 & F2 {${B}
        local r; local a = F1; local b = F2;
        ${code.format(i=0)}
        StoreClamp(DEST, DM, DC, r);
${E}}

:${op}.v2f16^M DEST^DC, F1_16, F2_16 is op=0xA5 & op2=${num} & M & DEST & DC & DM & F1_16 & F2_16 {${B}
        local res:4; local r:2; local a; local b;
% for st in (0, 16):
        a = F1_16[${st},16]; b = F2_16[${st},16];
        ${code.format(i=st)}
        res[${st},16] = r;
% endfor
        StoreClamp(DEST, DM, DC, res);
${E}}

% endfor

:V2F32_TO_V2F16^M DEST^DC, F1, F2 is op=0xA5 & op2=4 & M & DEST & DC & DM & F1 & F2 {${B}
        local res:4;
        res[0,16] = F1;
        res[16,16] = F2;
        StoreClamp(DEST, DM, DC, res);
${E}}

% for num, op, code in ((0, "FRCP.f32", "0x3F800000:4 f/ {}"), (1, "FRCP.f16", "0x3C00:2 f/ {}"), (2, "FRSQ.f32", "rsqrt({})"), (3, "FRSQ.f16", "rsqrt({})")):
<%
   bit_range = ""
   if op.endswith("u6"):
      src = "S1"
   elif op.endswith("f16"):
      src = "F1_16"
      bit_range = "[0,16]"
   else:
      src = "F1"
 %>

:${op}^M DEST, ${src} is op=0x9C & op2=${num} & M & DEST & DM & ${src} {${B}
        local res:4;
        res${bit_range} = ${code.format(src + bit_range)};
        Store(DEST, DM, res);
${E}}

% endfor


:IADD_IMM.32^M DEST, S1, #imm_32 is op=0x110 & M & DEST & DM & S1 & imm_32 {${B}
        Store(DEST, DM, S1 + imm_32);
${E}}


:FMA.f32^M DEST^DC, F1, F2, F3 is op=0xb2 & M & DEST & DC & DM & F1 & F2 & F3 {${B}
        StoreClamp(DEST, DM, DC, (F1 f* F2) f+ F3);
${E}}

:FMA.v2f16^M DEST^DC, F1_16, F2_16, F3_16 is op=0xb3 & M & DEST & DC & DM & F1_16 & F2_16 & F3_16 {${B}
        local res:4;
        res[0,16] = F1_16[0,16] f* F2_16[0,16] f+ F3_16[0,16];
        res[16,16] = F1_16[16,16] f* F2_16[16,16] f+ F3_16[16,16];
        StoreClamp(DEST, DM, DC, res);
${E}}

<%!
   sr_count = 0
   def make_sr(sec=False, reg=1, count=None):
      global sr_count
      if count == None:
         count = sr_count
      l = [f'sr{reg}_{x+1}' for x in range(count)]
      if sec:
         return " & ".join(l)
      else:
         return "@" + ":".join(l)
%>

% for count in range(1, 5):
<%
   global sr_count
   sr_count = count
 %>
% for st in range(1, 3):
SR${st}_${count}: ${make_sr(reg=st)} is ${make_sr(True, reg=st)} { export sr${st}_1; }
% endfor
% endfor

% for size in load_lanes:
% for num, lane in enumerate(load_lanes[size]):
% if lane:
LOAD_LANE_${size}: ".${lane}" is load_lane=${num} { export ${num}:1; }
% else:
LOAD_LANE_${size}: "" is load_lane=${num} { export ${num}:1; }
% endif
% endfor

% endfor

% for sr_count in range(1, 5):

# TODO: Variant for each modifier configuration?
:TEX^M^EXPLICIT^SHADOW^LOD_MODE^DIMENSION^SKIP ${make_sr()}, SR2_4, S1
is op=0x128 & M & EXPLICIT & SHADOW & LOD_MODE & DIMENSION & SKIP & ${make_sr(True)} & SR2_4 & S1 & sr_count=${sr_count} unimpl

:ATEST^M ${make_sr()}, S1, SW2_swz32, S3 is op=0x7D & M & ${make_sr(True)} & S1 & SW2_swz32 & S3 & sr_count=${sr_count} {${B}
% if sr_count:
        sr1_1 =
% endif
        atest(S1, SW2_swz32, S3);
${E}}

:BLEND^M^VECSIZE^REGFMT ${make_sr()}, ${make_sr(reg=2, count=1)}, S1, "target":blend_target is op=0x7f & M & VECSIZE & REGFMT & ${make_sr(True)} & ${make_sr(sec=True, reg=2, count=1)} & S1 & blend_target & sr_count=${sr_count} {${B}
        local tmp:4 = 0;

        if ((REGFMT & 1:1) == 1:1) goto <f16>;
% for sz in range(2, 5):
        if (VECSIZE == ${sz}:1) goto <b32_${sz}>;
% endfor
% for sz in range(1, 5):
% if sz != 1:
      <b32_${sz}>
% endif
        blend_${"rgba"[:sz]}(${", ".join([f"sr1_{x + 1}" for x in range(min(sz, sr_count))])});
        goto <end>;
% endfor

      <f16>
% for sz in range(2, 5):
        if (VECSIZE == ${sz}:1) goto <b16_${sz}>;
% endfor
% for sz in range(1, 5):
% if sz != 1:
      <b16_${sz}>
% endif
        blend_${"rgba"[:sz]}(${", ".join([f"sr1_{x//2 + 1}[{(x & 1) * 16},16]" for x in range(min(sz, sr_count * 2))])});
        goto <end>;
% endfor

      <end>
        sr2_1 = blend_coverage();
${E}}

# TODO: Share code between all these instructions

:LD_ATTR_IMM^M^VECSIZE^REGFMT ${make_sr()}, S1, S2, "index":ld_var_index is op=0x66 & M & VECSIZE & REGFMT & ${make_sr(True)} & S1 & S2 & ld_var_index & sr_count=${sr_count} {${B}
% for i in range(sr_count):
        sr1_${i+1} = load_attr(S1, S2, ld_var_index + ${i}:4);
% endfor
${E}}

:LD_ATTR^M^VECSIZE^REGFMT ${make_sr()}, S1, S2, S3 is op=0x76 & M & VECSIZE & REGFMT & ${make_sr(True)} & S1 & S2 & S3 & sr_count=${sr_count} {${B}
% for i in range(sr_count):
        sr1_${i+1} = load_attr(S1, S2, S3 + ${i}:4);
% endfor
${E}}

:LD_VAR_IMM_F32^M^VECSIZE^REGFMT ${make_sr()}, S1, S2, "index":ld_var_index is op=0x5c & M & VECSIZE & REGFMT & ${make_sr(True)} & S1 & S2 & ld_var_index & sr_count=${sr_count} {${B}
# TODO: do something with VECSIZE and REGFMT
# TODO: Is addition to ld_var_index correct?
% for i in range(sr_count):
        sr1_${i+1} = load_var(S1, S2, ld_var_index + ${i}:4);
% endfor
${E}}

:LD_VAR_IMM_F16^M^VECSIZE^REGFMT ${make_sr()}, S1, S2, "index":ld_var_index is op=0x5d & M & VECSIZE & REGFMT & ${make_sr(True)} & S1 & S2 & ld_var_index & sr_count=${sr_count} {${B}
# TODO: do something with VECSIZE and REGFMT
# TODO: Is addition to ld_var_index correct?
% for i in range(sr_count):
        sr1_${i+1} = load_var(S1, S2, ld_var_index + ${i}:4);
% endfor
${E}}

:LEA_ATTR^M ${make_sr()}, S1, "unk":lea_imm is op=0x5E & M & ${make_sr(True)} & S1 & lea_imm & sr_count=${sr_count} {${B}
       # TODO: Add more hidden CPU registers to handle this..
       sr1_1 = S1 * 4:4;
% if sr_count > 1:
       sr1_2 = 0xFADE0000:4 + lea_imm;
% endif
${E}}

:LEA_IMAGE^M^VECSIZE ${make_sr()}, SW1_32, S2, S3 is op=0x77 & M & VECSIZE & ${make_sr(True)} & SW1_32 & S2 & S3 & sr_count=${sr_count} {${B}
% for i in range(sr_count):
        sr1_${i+1} = lea_image(SW1_32, S2, S3);
% endfor
${E}}

:ST_CVT^M^VECSIZE^REGFMT ${make_sr()}, S1, S2, S3, "unk":unk36 is op=0x71 & M & VECSIZE & REGFMT & ${make_sr(True)} & S1 & S2 & S3 & unk36 & sr_count=${sr_count} {${B}
% for i in range(sr_count):
        store_cvt(sr1_${i+1}, S1, S2, S3, unk36:8);
% endfor
${E}}

<%!
   def lane_check(width, comp, num):
      if width == 8:
          return f"LOAD_LANE_8 {comp} {num + 4}"
      elif width == 16:
          return f"LOAD_LANE_16 {comp} {num}"
 %>

% for width, op2, *lane in ((8, 0), (16, 1), (24, 2, 0), (32, 3), (48, 4, 4), (64, 5, 7), (96, 6, 6), (128, 7, 7)):
<%
   if len(lane):
      print_lane = ""
      oper_lane = f" & load_lane={lane[0]}"
   else:
      print_lane = f"^LOAD_LANE_{width}"
      oper_lane = f" & LOAD_LANE_{width}"
 %>

% for ld_name, ld_op in (("LOAD", 0x60), ("LD_BUFFER", 0x6a)):
<% ld_addr = "LdstAddr" if ld_name == "LOAD" else "BufferAddr" %>
:${ld_name}.^SIGN^${width}^M ${make_sr()}${print_lane}, ${ld_addr} is op=${ld_op} & op2_ldst=${op2} & M & SIGN & ${make_sr(True)} & ${ld_addr} & sr_count=${sr_count}${oper_lane} {${B}
% for i in range(min(sr_count, width // 32)):
        sr1_${i+1} = *:4 (${ld_addr} + ${4 * i}:8);
% endfor
% if width == 32:
% if sr_count > 1:
        if (LOAD_LANE_32 == 0) goto <noext>;
        if (SIGN == 0) goto <unsigned>;
        sr1_2 = sr1_1 s>> 31;
        goto <noext>;
      <unsigned>
        sr1_2 = 0:4;
      <noext>
% endif
% elif (width == 24 or width == 48) and width // 32 < sr_count:
        if (SIGN == 0) goto <unsigned>;
        sr1_${width // 32 + 1} = sext(*:${(width // 8) % 4} (${ld_addr} + ${4 * (width // 32)}:8));
        goto <end>;
      <unsigned>
        sr1_${width // 32 + 1} = zext(*:${(width // 8) % 4} (${ld_addr} + ${4 * (width // 32)}:8));
      <end>
% elif print_lane:
% if width == 8:
        if (LOAD_LANE_8 >= 4) goto <notbyte>;
        local shift8:1 = LOAD_LANE_8 * 8:1;
        sr1_1 = (sr1_1 & ~(0xff:4 << shift8)) | (zext(*:1 ${ld_addr}) << shift8);
        goto <end>;
      <notbyte>
% endif
        if (${lane_check(width, "<", 2)}) goto <half>;
        if (SIGN == 0) goto <unsigned>;
        sr1_1 = sext(*:${width // 8} ${ld_addr}:8);
        goto <check64>;
      <unsigned>
        sr1_1 = zext(*:${width // 8} ${ld_addr}:8);
      <check64>
        if (${lane_check(width, "==", 2)}) goto <end>;
% if sr_count > 1:
        if (SIGN == 0) goto <u64>;
        sr1_2 = sr1_1 s>> 31;
        goto <end>;
      <u64>
        sr1_2 = 0:4;
% endif
        goto <end>;
      <half>
        local shift:1 = (LOAD_LANE_${width}${" - 4:1" if width==8 else ""}) * 16:1;
        local val:2 = sext(*:${width // 8} ${ld_addr});
        local valu:4 = zext(val);
        sr1_1 = (sr1_1 & ~(0xffff:4 << shift)) | (valu << shift);
      <end>
% endif
${E}}
% endfor

# TODO: Handle store_segment modifier

:STORE.i${width}^M ${make_sr()}, LdstAddr is op=0x61 & op2_ldst=${op2} & M & ${make_sr(True)} & LdstAddr & sr_count=${sr_count} {${B}
% for i in range(min(sr_count, width // 32)):
        *:4 (LdstAddr + ${i * 4}:8) = sr1_${i + 1};
% endfor
% if width % 32 and width // 32 < sr_count:
        *:${(width // 8) % 4} (LdstAddr + ${4 * (width // 32)}:8) = sr1_${width // 32 + 1}:${(width // 8) % 4};
% endif
${E}}

% endfor
% endfor

RelBranchz: loc is branchz_ofs [ loc = inst_next + branchz_ofs * 8; ] { export *:1 loc; }

:BRANCHZ M^EQ SW1_lane32, RelBranchz ("offset:"^branchz_ofs) is op=0x1f & M & EQ & SW1_lane32 & RelBranchz & branchz_ofs {${B}
        if ((SW1_lane32 == 0) == EQ)
                goto RelBranchz;
${E}}

S2PTR: S2 is S2 { local addr:8 = zext(S2); export *:1 addr; }

:BRANCHZI M^EQ S1, S2PTR is op=0x2f & M & EQ & S1 & S2PTR {${B}
# TODO: Why does this cause problems for the decompiler?
#        if ((S1 == 0) == EQ)
#                goto S2PTR;
        branchzi((S1 == 0) == EQ, S2PTR);
${E}}

# TODO: Do masks work differently for 64-bit operations?
:SHADDX.u64^M DEST, S1_64, SW2_64, "shift":mod_shift is op=0x1A3 & op2=7 & M & DEST & D1_64 & DM & S1_64 & SW2_64 & mod_shift {${B}
        local b:8 = SW2_64;
        local res:8 = S1_64 + (b << mod_shift);
        Store64(D1_64, DM, res);
${E}}

# TODO: Do masks work differently for 64-bit operations?
# TODO: sign-extend
:SHADDX.s64^M DEST, S1_64, "shift":mod_shift is op=0x1AB & op2=7 & M & DEST & D1_64 & DM & S1_64 & mod_shift {${B}
#        local b:8 = SW2_64;
#        local res:8 = S1_64 + (b << mod_shift);
#        Store64(D1_64, DM, res);
${E}}

% for op2, name, src, roundm, r_code in convert:
% for rnum, (r, rop) in enumerate(round_mode) if roundm else ((0, (0, 0)),):
<%
   if roundm:
      r_op = r
      r_oper = f" & mod_round={rnum}"
      code = r_code.replace("round", rop)
   else:
      r_op = ""
      r_oper = ""
      code = r_code
 %>

:${name}${r_op}^M DEST, ${src} is op=0x90 & op2_cvt=${op2} & M & DEST & DM & ${src}${r_oper}
{${B}
% if code[0] == ":":
        local val:${code[1]} =${code[2:].format(src)};
        Store(DEST, DM, zext(val));
% elif "local val" in code:
        ${code.format(src)};
        Store(DEST, DM, zext(val));
% else:
        Store(DEST, DM, ${code.format(src)});
% endif
${E}}

% endfor
% endfor

:MKVEC.v2i16^M DEST, SW1_16, SW2_16 is op=0xa1 & op2=0x5 & M & DEST & DM & SW1_16 & SW2_16 {${B}
        local res:4;
        res[0,16] = SW1_16;
        res[16,16] = SW2_16;
        Store(DEST, DM, res);
${E}}

## Shift instructions

# TODO: Do unsigned shifts exist?
% for name, op, code in (("AND", 0, "&"), ("OR", 1, "|"), ("XOR", 2, "^")):
% for dir, shift_op, shift_code in (("L", 1, "<<"), ("R", 0, "s>>")):
% for size, comps, bits, sz_op in ((".i32", 1, 32, 0xb4), (".v2i16", 2, 16, 0xb5), (".v4i8", 4, 8, 0xb6)):

:${dir}SHIFT_${name}${size}^M^NOT_RES DEST, SW1_${bits}, SW2_lane8, S3^NOT_SRC is op=${sz_op} & op2_shift=${op} & mod_left=${shift_op} & M & NOT_RES & DEST & DM & SW1_${bits} & SW2_lane8 & S3 & NOT_SRC {${B}

        local S3_val:4 = S3;
        if (NOT_SRC == 0:1) goto <keep_S3_val>;
        S3_val = ~S3_val;
      <keep_S3_val>

        local res:4 = 0;
% for i in range(comps):
<%
   bit_range = "" if bits==32 else f"[{bits * i},{bits}]"
   # TODO: How does the shift amount work for v4i8 shifts?
   shift_src = f"SW2_lane8[{16 if i & 1 else 0},8]"
 %>        res${bit_range} = SW1_${bits}${bit_range} ${shift_code} ${shift_src};
% endfor
        res = res ${code} S3_val;
        if (NOT_RES == 0:1) goto <keep_res>;
        res = ~res;
      <keep_res>
        Store(DEST, DM, res);
${E}}
% endfor

# TODO: 64-bit instructions
# TODO: What bitness is the second source for .i64?

% endfor
% endfor

% for comp, mod_cmp, *ops in compare:
% for size, comps, bits, sz_op, typ in ((".u32", 1, 32, 0, 0), (".v2u16", 2, 16, 1, 0), (".v4u8", 4, 8, 2, 0), (".i32", 1, 32, 8, 1), (".v2i16", 2, 16, 9, 1), (".v4s8", 4, 8, 10, 1), (".f32", 1, 32, 4, 2), (".v2f16", 2, 16, 5, 2)):

<%
   restype = "RESTYPE" + ("_16" if bits == 16 else "")
   if typ == 2 and bits == 32:
      src = "F{}"
   elif typ == 2 and bits == 16:
      src = "F{}_16"
   else:
      src = f"SW{{}}_{bits}"

   src1 = src.format(1)
   src2 = src.format(2)
 %>

# TODO: seq modifier
# TODO: Share code with csel definition?
:${"F" if typ == 2 else "I"}CMP${size}^M^"${comp}"^${restype}^AND DEST, ${src1}, ${src2}, S3 is op=${0xf0 + sz_op} & M & mod_cmp=${mod_cmp} & ${restype} & AND & DEST & DM & ${src1} & ${src2} & S3
{${B}
        local res:4 = 0;
% for i in range(comps):
<%
   code = ops[typ]
   if not "{" in code:
        code = "{a} " + code + " {b}"
   bit_range = "" if bits==32 else f"[{bits * i},{bits}]"
   code = code.format(a=src1, b=src2)
   res_range = ":1" if bits==8 else ""
 %>
        if (${code}) goto <t${i}>;
        res${bit_range} = 0;
        goto <e${i}>;
      <t${i}>
        res${bit_range} = ${restype + res_range};
      <e${i}>
% endfor
        if (AND == 1:1) goto <and>;
        res = res | S3;
        goto <end>;
      <and>
        res = res & S3;
      <end>
        Store(DEST, DM, res);
${E}}

:CSEL${size}^M^"${comp}" DEST, S1, S2, S3, S4 is op=${0x150 + sz_op} & M & mod_cmp=${mod_cmp} & DEST & DM & S1 & S2 & S3 & S4 {${B}
        local res:4 = 0;
% for i in range(comps):
<%
   code = ops[typ]
   if not "{" in code:
        code = "{a} " + code + " {b}"
   bit_range = "" if bits==32 else f"[{bits * i},{bits}]"
   code = code.format(a="S1" + bit_range, b="S2" + bit_range)
 %>
        if (${code}) goto <t${i}>;
        res${bit_range} = S4${bit_range};
        goto <e${i}>;
      <t${i}>
        res${bit_range} = S3${bit_range};
      <e${i}>
% endfor
        Store(DEST, DM, res);
${E}}

% endfor
% endfor
